//Burger_menu
const menuButton = document.querySelector(".header__menu-button");
const menuList = document.querySelector(".header__menu--hidden");
// menuButton.addEventListener("click", () => {
//   menuButton.classList.toggle("burger-btn-close");
//   if (!menuButton.classList.contains(".header__menu-button burger-btn-close")) {
//     document
//       .querySelector(".header__menu")
//       .classList.toggle("header__menu--hidden");
//   }
// });
document.body.addEventListener(
  "click",
  (event) => {
    event.preventDefault();
    if (event.target.classList.contains("menu-open")) {
      menuButton.classList.toggle("burger-btn-close");
      document
        .querySelector(".header__menu")
        .classList.toggle("header__menu--hidden");
    } else {
      menuButton.classList.remove("burger-btn-close");
      document
        .querySelector(".header__menu")
        .classList.add("header__menu--hidden");
    }
  }
  // if (!event.target.classList.contains(".header__menu") && !event.target.classList.contains(".header__menu-button")) {
  //   document
  //     .querySelector(".header__menu")
  //     .classList.add("header__menu--hidden");
  // }
);
